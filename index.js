const fs = require('fs')
const parse = require('./parser')
const convert = require('./converter')

const fileName = getFileNameFromTheArguments()
if (!isFileNameValid(fileName)) {
    returnFileNameError()
}

const fileData = readFile(fileName)
if (!isFileDataValid(fileData)) {
    returnFileDataError()
}

const trades = parse(fileData)
const csv = convert(trades)
console.log(csv)


function getFileNameFromTheArguments () {
    const ARGUMENTS = {
        NODE: 0,
        SCRIPT: 1,
        FILENAME: 2
    }

    return process.argv[ARGUMENTS.FILENAME]
}

function readFile (fileName) {
    let fileData
    try {
        fileData = fs.readFileSync(fileName).toString()
    } catch (error) {
        returnReadingFileError(error)
    }
    return fileData
}

function isFileNameValid (fileName) {
    return fileName
}

function isFileDataValid (fileData) {
    const RAW_HEADER = '"txid","refid","time","type","subtype","aclass","asset","amount","fee","balance"'

    if (!fileData) return false

    const splitted = fileData.split('\r\n')
    if (!splitted.length) return false

    if (splitted[0] !== RAW_HEADER) return false

    return true
}

function returnFileNameError () {
    console.error('Correct use: node index.js [filename]')
    process.exit(1)
}

function returnReadingFileError (error) {
    console.error(error)
    console.error('There was an error reading the file')
    process.exit(2)
}

function returnFileDataError () {
    console.error('The entered file is not correct. Export it from Kraken (Ledgers, all options)')
    process.exit(3)
}