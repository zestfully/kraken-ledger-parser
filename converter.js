const HEADER = 'Date,Exchange,BuyAmount,BuyCurrency,SellAmount,SellCurrency,FeeAmount,FeeCurrency'
const RETURN_HEADER = true

const convertTradesToCsv = (trades) => {
    let csv = ''

    if (RETURN_HEADER) {
        csv += HEADER
        csv += '\n'
    }

    const sortedTrades = sortTradesByDate(trades)
    const convertedTrades = sortedTrades.map(convertTradeToCsv)
    const joinedTrades = convertedTrades.join('\n')

    csv += joinedTrades

    return csv
}

function convertTradeToCsv (trade) {
    let csv = ''
    
    csv += `${trade.date.toISOString()},`
    csv += `${trade.exchange},`
    csv += `${trade.buyAmount},`
    csv += `${trade.buyCurrency},`
    csv += `${trade.sellAmount},`
    csv += `${trade.sellCurrency},`
    csv += `${trade.feeAmount},`
    csv += `${trade.feeCurrency}`

    return csv
}

function sortTradesByDate (trades) {
    return trades.sort((a, b) => a.date - b.date)
}

module.exports = convertTradesToCsv
