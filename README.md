# How to use
```
node index.js ledgers.csv
```

It'll print a parsed CSV. Use a redirection to save it.
```
node index.js ledgers.csv > parsed.csv
```
